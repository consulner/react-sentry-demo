# react-sentry-demo

Demonstration of sentry/react integration

References: 
- [Link sentry with release version](https://docs.sentry.io/workflow/releases/?_ga=2.200611308.315665802.1585665926-289999655.1585069805&platform=javascript#create-release)
- [Configure sentry-cli](https://docs.sentry.io/cli/configuration/)
- [Source maps](https://docs.sentry.io/platforms/javascript/sourcemaps/)
- Repository integration:
 - [Create release endpoint](https://docs.sentry.io/workflow/releases/?_ga=2.264715725.315665802.1585665926-289999655.1585069805%3F_ga&platform=javascript#alternatively-without-a-repository-integration)
 - [Plugin](https://sentry.io/integrations/gitlab/)

```
$ sentry-cli releases files <release_name> upload-sourcemaps /path/to/files

This command will upload all files ending in .js and .map to the specified release. If you wish to change these extensions – for example, to upload typescript sources – use the --ext option:
```