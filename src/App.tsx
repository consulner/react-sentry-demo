import React from 'react'
import * as Sentry from '@sentry/browser';

const fail = () => { throw new Error("Failed!!") };

const captureError = () => {
    try {
        throw new Error("Expected error");
    } catch (error) {
        Sentry.captureException(error);
    }
}

const sendMessage = () => {
    Sentry.captureMessage(`Example message`);
}

export const App: React.FunctionComponent = () => {
    return (
        <div className="ui container">
            <h1>Sentry demo</h1>
            <button className="ui negative basic button" onClick={fail}>Collapse the World!</button>
            <button className="ui green basic button" onClick={captureError}>Capture error</button>
            <button className="ui yellow basic button" onClick={sendMessage}>Send message</button>
        </div>
    );
}
