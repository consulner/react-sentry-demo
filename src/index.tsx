import React from 'react';
import ReactDOM from 'react-dom';
import * as Sentry from '@sentry/browser';

import { App } from './App';

Sentry.init({dsn: "https://8add04be9733a48f786a98e77cac254d@sentry.io/4517353"});

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);